# nginx state for serving content
FROM nginx:1.21.6-alpine
LABEL maintainer="shaiful"

# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

# Copy static assets from builder stage
#COPY dist/somch-employee-portal-web .

COPY nginx.conf /etc/nginx/conf.d/default.conf

# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]

#sudo docker build -t shaiful365/somch-employee-portal-web:0.0.1 .
#sudo docker image rm  shaiful365/somch-employee-portal-web:0.0.1
# sudo DOCKER_BUILDKIT=1 docker image build -t shaiful365/somch-employee-portal-web:0.0.1 --no-cache .
