jQuery(document).ready(function () {
  console.log("Log from global script");
  jQuery(".left-menu-trigger button").on("click", function () {
    jQuery("body").toggleClass("side-menu-activated");
    //jQuery(".leftside-menu").toggleClass("side-menu-activated");
  });
  jQuery(document).on('click', '.leftside-menu-wrapper > ul > li > a', function() {
    jQuery(this).parent().siblings().removeClass("activated");
    jQuery(this).parent().addClass("activated");
    let parentMenuItem =  jQuery(this).parent().attr("id");
    localStorage.setItem('current-module', parentMenuItem);
});
 


  jQuery("#patient-reg-type").on("change", function () {
    const selectedItem = $("#patient-reg-type option:selected").val();
    if (selectedItem === "general") {
      jQuery("#patient-reg-doc").hide();
    } else {
      jQuery("#patient-reg-doc").show();
    }
  });
  jQuery("#toggleSwitch").on("change", function (event) {
    if (event.target.checked) {
      jQuery("#toggleText").text("MIX");
    } else {
      jQuery("#toggleText").text("ADD");
    }
  });
});
