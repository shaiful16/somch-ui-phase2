jQuery(document).ready(function () {
  // const BASE_URL = 'http://127.0.0.1:5500/';
  //const BASE_URL = 'http://localhost:63342/somch-ui-phase2';
  // Live server
  const BASE_URL = "http://119.148.6.90:9177";
  var url = jQuery(location).attr("href");
  const leftMenu = `
        <ul>
                         
            <li class="parent-menu-item" id="dashboard-menu">
                <a href="${BASE_URL}"> 
                    <span class="icon icon-35x icon-dashboard"></span>
                    <span>Dashboard</span> 
                    <span></span> 
                </a>
             </li>
             
              <li class="parent-menu-item" id="prescription-new">
                <a href="${BASE_URL}"> 
                    <span class="icon icon-35x icon-dashboard"></span>
                    <span>prescription</span> 
                    <span></span> 
                </a>
                <ul>                 
                    <li> <a href="${BASE_URL}/prescription-engine-general.html"> prescription-new</a> </li>
                    </ul>
             </li>

            <li class="parent-menu-item" id="emergency-menu-parent"> 
                 <a href="#"> 
                     <span class="icon icon-35x icon-emergency"></span>
                     <span>Emergency</span>
                     <span class="fa"></span>
                 </a>
                <ul>
                    <li> <a href="${BASE_URL}/emergency/admission-html/emergency-adm-req-list.html"> Emergency Admission </a> </li> 
                    <li> <a href="${BASE_URL}/emergency/nurse-html/01-emergency-ns-patient-rcv-rqst.html"> Nurse Station </a> </li>
                    <li> <a href="${BASE_URL}/emergency/nurse-html/55-emergency-ns-bbr-return.html"> Blood Return </a> </li>
                    <li> <a href="${BASE_URL}/emergency/nurse-html/61-emergency-item-consumption.html"> Medicine Consumption </a> </li>
                    <li> <a href="${BASE_URL}/emergency/nurse-html/63-emergency-item-consumption.html"> Consumption (Others)</a> </li>
                    <li> <a href="${BASE_URL}/emergency/nurse-html/62-emergency-ns-dis-req-patient-list.html"> Discharge Requested Patient List </a> </li>
                    <li> <a href="${BASE_URL}/emergency/doctor-html/01.0-ipd-ds-adm-patient-list.html"> Doctor Station </a> </li>
                    <li> <a href="${BASE_URL}/emergency/business-ofc-html/ipd-business-patient-list.html"> Bill Counter </a> </li>
                    <li> <a href="${BASE_URL}/emergency/business-ofc-html/bill-settlement.html"> Financial Settlement</a> </li>
                    <li> <a href="${BASE_URL}/emergency/business-ofc-html/advance-deposit-collection.html"> Advance Collection</a> </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="ipd-menu-parent">
              <a href="#"> 
                <span class="icon icon-35x icon-ipd"> </span> 
                <span>In-Patient Dep. (IPD) </span> 
                <span class="fa"></span>
              </a>
                <ul>                 
                    <li> <a href="${BASE_URL}/ipd/doctor-html/01.0-ipd-ds-adm-patient-list.html"> Doctor Station </a> </li>
                    <li> <a href="${BASE_URL}/ipd/doctor-html/10.0-ipd-ds-ot-req.html"> OT Requisition </a> </li>
                    <li> <a href="${BASE_URL}/ipd/nurse-html/01-ipd-ns-patient-rcv-rqst.html"> Nurse Station </a> </li>
                    <li> <a href="${BASE_URL}/ipd/nurse-html/55-ipd-ns-bbr-return.html"> Blood Return </a> </li>
                    <li> <a href="${BASE_URL}/ipd/doctor-html/12-ipd-payingbed-req-approval.html">Requisition Approval (Paying Bed) </a> </li>
                    <li> <a href="${BASE_URL}/ipd/nurse-html/61-ipd-item-consumption.html"> Medicine  Consumption </a> </li>
                    <li> <a href="${BASE_URL}/ipd/nurse-html/63-ipd-item-consumption-others.html">  Consumption (Other) </a> </li>
                    <li> <a href="${BASE_URL}/ipd/nurse-html/62-ipd-ns-dis-req-patient-list.html"> Discharge Requested Patient List </a> </li>
                    <li> <a href="${BASE_URL}/ipd/business-ofc-html/ipd-business-patient-list.html"> Bill Countert</a> </li>
                    <li> <a href="${BASE_URL}/ipd/business-ofc-html/bill-settlement.html"> Financial Settlement</a> </li>
                    <li> <a href="${BASE_URL}/ipd/business-ofc-html/advance-deposit-collection.html"> Advance Collection</a> </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="opd-menu-parent">
                <a href="#">
                  <span class="icon icon-35x icon-outdoor"></span>
                  <span>Other Services</span> 
                  <span class="fa"></span>
                </a>
                <ul>
                  <li>
                    <a href="${BASE_URL}/opd-others/2-opd_others_service_payment.html"> Others Service Patient List</a>
                  </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="ot-menu-parent">
                <a href="#">
                  <span class="icon icon-35x icon-ot"></span>
                  <span> Operation  Theater (OT)</span> 
                  <span class="fa"></span>
                </a>
                <ul>
                  <li><a href="${BASE_URL}/ot/01.0-ot-otrq-rql.html">OT Requisition</a></li>
                  <li><a href="${BASE_URL}/ot/16.0-OT-OTSH.html">OT Room Schedule</a></li>
                </ul>
            </li>
            <li class="parent-menu-item" id="minor-ot-menu-parent">
                <a href="#"> <span class="icon icon-35x icon-minor-ot"></span> <span>Minor OT </span> <span
                        class="fa"></span></a>
                <ul>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-billing.html">Billing</a></li>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-worklist.html"> Minor OT List</a></li>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-med-req.html"> Medicine Requisition</a></li>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-general-req.html"> General Item Requisition</a></li>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-item-consumption.html">Medicine  Consumption</a></li>
                    <li><a href="${BASE_URL}/minor-ot/minor-ot-item-consumption-others.html">Others Consumption</a></li>
                    <li class="activated-submenu">
                        <a href="">Reports</a>
                        <ul>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-report-gopd-peramiter.html">GOPD Report</a></li>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-report-operation-list-gopd-perameter.html">Minor Operation
                                List- GOPD</a></li>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-report-total-opd-adm-operation-peramiter.html">Total OPD
                                Admission & Operation</a></li>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-report-examination-peramiter.html">Examination List</a></li>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-report-via-peramiter.html">VIA Register</a></li>
                        </ul>
                    </li>
                    <li class="activated-submenu">
                        <a href="">Setup</a>
                        <ul>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-setup-lookup.html">Lookup Setup</a></li>
                            <li><a href="${BASE_URL}/minor-ot/minor-ot-setup-test.html">Test Setup</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="icu-menu-parent">
                <a href="#"> 
                    <span class="icon icon-35x icon-icu-ccu"></span>
                    <span> ICU/CCU Management </span>
                    <span class="fa"></span>
                </a>
                <ul>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-nurse-rec-patient-adm-list.html" > Nurse Station </a>  </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-nurse-rec-pat-details-update.html" > Nurse Station Details </a>  </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/55-emergency-ns-bbr-return.html"> Blood Return </a></li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/61-emergency-item-consumption.html">  Medicine  Consumption </a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/63-ipd-item-consumption-others.html">  Consumption (Other) </a> </li>
                  
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-doctor-station.html"> Doctor Station </a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/10.0-ipd-ds-ot-req.html"> OT Requisition Approval </a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/12-ipd-payingbed-req-approval.html">Requisition Approval (Paying Bed) </a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/62-emergency-ns-dis-req-patient-list.html" > Discharge Requested Patient List</a> </li>
                  <li><a href="${BASE_URL}/icu-ccu-hdu/ccu-icu_billing.html"> Billing </a></li>
                  <li><a href="${BASE_URL}/icu-ccu-hdu/ccu-icu_bill-cancellation.html"> Bill Cancellation </a></li>
                  
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/ipd-business-patient-list.html"> Bill Countert</a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/bill-settlement.html"> Financial Settlement</a> </li>
                  <li> <a href="${BASE_URL}/icu-ccu-hdu/advance-deposit-collection.html"> Advance Collection</a> </li>

                  <li class="activated-submenu">
                    <a href="#">Reports</a>
                    <ul>
                      <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report_indoor-investigation.html"> Indoor Investigation Details Information </a> </li>                  
                      <li><a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report_out-sample.html"> Out Sample Information </a> </li>
                      <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report_daily-patient.html"> Daily Patient Statement </a></li>
                      <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report_test-wise-information.html"> Test Wise Information </a> </li>
                      <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report_on-call-statement.html"> On Call Statement  </a> </li>
                      <li> <a href="${BASE_URL}/icu-ccu-hdu/ccu-icu-report-admitted-patient-list.html"> Admitted Patient List </a></li>
                    </ul>
                  </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="death-reg-menu-parent">
                <a href="#">
                    <span class="icon icon-35x icon-death"></span>
                    <span>Death Registration</span> 
                    <span class="fa"></span>
                </a>
                <ul>
                    <li> <a href="${BASE_URL}/death-registration/1-death-registration-death-patient-list.html">Death Patient List</a> </li>
                    <li> <a href="${BASE_URL}/death-registration/2-death-registration-death-certificate-issue.html">Death Certificate Issue</a> </li>
                    <li> <a href="${BASE_URL}/death-registration/3-death-registration-police-handover-list.html">Police Handover List</a> </li>
                    <li class="activated-submenu"><a href="#">Report</a>
                        <ul>
                            <li><a href="${BASE_URL}/death-registration/6-death-registration-report-patient-list.html">Death Patient List</a> </li>
                            <li><a href="${BASE_URL}/death-registration/7-death-registration-report-type-wise-death-patient-list.html">Type  Wise Death Patient List</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="blood-bank-menu-parent">
                <a href="#">
                    <span class="icon icon-35x icon-bloodbank"></span>
                    <span>Blood Bank</span>
                    <span class="fa"></span>
                </a>
                <ul>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-requisition-list.html">Requisition List</a></li>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-donor-registration.html">Donor Registration</a></li>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-screening-test-donor.html">Medical Checkup</a></li>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-blood-collection.html">Blood Collection</a></li>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-blood-issue.html">Blood Allotment</a></li>
                    <li><a href="${BASE_URL}/blood-bank/blood-bank-stock-register.html">Stock Register</a></li>
                    <li class="activated-submenu"><a href="${BASE_URL}/blood-bank/blood-bank-reports.html">Reports</a>
                        <ul>
                            <li><a href="${BASE_URL}/blood-bank/blood-bank-test-wise-information-reports.html">Test Wise  Information</a></li>
                            <li><a href="${BASE_URL}/blood-bank/blood-bank-stock-register-reports.html">Stock Register</a></li>
                        </ul>
                    </li>
        
                </ul>
            </li>
            <li class="parent-menu-item" id="ambulance-menu-parent">
                <a href="#"> 
                    <span class="icon icon-35x icon-ambulance"></span> 
                    <span>Ambulance Management </span> 
                    <span class="fa"></span>
                </a>
                <ul>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-vehicle-assign.html">Vehicle Assign</a>  </li>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-vehicle-list.html">Vehicle List</a>  </li>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-requisition-pending.html"> Ambulance Requisition </a></li>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-roaster.html"> Ambulance Roaster </a></li>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-fuel-consumption.html"> Fuel Consumption </a></li>
                  <li> <a href="${BASE_URL}/ambulance/ambulance-fuel-refill.html">Fuel Refill</a> </li>

                  <li class="activated-submenu">
                    <a href="#">Setup</a>
                    <ul>
                      <li> <a href="${BASE_URL}/ambulance/ambulance-vehicle-setup.html">Ambulance Setup </a></li>
                      <li> <a href="${BASE_URL}/ambulance/ambulance-destination-setup.html"> Destination Setup </a></li>
                    </ul>
                  </li>
                  <li class="activated-submenu">
                    <a href="">Report</a>
                    <ul>
                      <li> <a href="${BASE_URL}/ambulance/ambulance-parameter-payment-summary.html"> Payment Summary </a></li>
                      <li> <a href="${BASE_URL}/ambulance/ambulance-parameter-bill-fuel-refill.html"> Monthly Bill of Fuel Refill </a></li>
                      <li> <a href="${BASE_URL}/ambulance/ambulance-parameter-vehicle-Logbook.html"> Vehicle Logbook </a></li>
                    </ul>
                  </li>
                </ul>
              </li> 
            <li class="parent-menu-item" id="telemedicine-menu-parent">
                <a href="#">
                  <span class="icon icon-35x icon-telemedicine"> </span>
                  <span>Telemedicine </span>
                  <span class="fa"></span>
                </a>
                <ul>
                  <li> <a href="${BASE_URL}/telemedicine/1-0-telemedicine-room.html"> Telemedicine Room </a> </li>
                  <li> <a href="${BASE_URL}//telemedicine/2-0-telemedicine-room-in-prescription.html"> Telemedicine  Prescription </a> </li>
                  <li> <a href="${BASE_URL}/telemedicine/3-0-telemedicine-patient-registration.html"> Patient Registration </a></li>
                  <li> <a href="${BASE_URL}/telemedicine/4-0-telemedicine-prescription-list.html">Telemedicine Prescription List </a></li>
                  <li> <a href="${BASE_URL}/telemedicine/4-5-telemedicine-patient-list.html">Telemedicine Patient List </a></li>

                  <li class="activated-submenu">
                    <a href="#">Setup</a>
                    <ul>
                      <li> <a href="${BASE_URL}/telemedicine/5-0-telemedicine-setup-email.html"> Email Setup</a></li>
                      <li> <a href="${BASE_URL}/telemedicine/6-0-telemedicine-setup-schedule.html"> Destination Setup </a></li>
                    </ul>
                  </li>                
                </ul>
             </li>          
            <li class="parent-menu-item" id="laundry-menu-parent">
                <a href="#"> 
                  <span class="icon icon-35x icon-laundry"> </span>
                  <span>Laundry Management </span>
                  <span class="fa"></span>
                </a>
                <ul>
                  <li class="activated-submenu">
                    <a href="#">Receive Requisition</a>
                    <ul>
                      <li> <a href="${BASE_URL}/laundry/laundry-item-req.html"> Item Requisition </a> </li>
                      <li> <a href="${BASE_URL}/laundry/laundry-requisition-verify.html"> Requisition Verify </a> </li>
                    </ul>
                  </li>
    
            
                  
                  <li class="activated-submenu">
                    <a href="#">Send Requisition</a>
                    <ul>
                      <li> <a href="${BASE_URL}/laundry/laundry-send-req-condemn-req.html"> Condemn Requisition </a> </li>
                      <li> <a href="${BASE_URL}/laundry/laundry-send-req-condemn-verify.html"> Condemn Verify </a> </li>
                    </ul>
                  </li>
                </ul>
              </li>     
            <li class="parent-menu-item" id="linen-menu-parent">
                    <a href="#">
                      <span class="icon icon-35x icon-linen"></span>
                      <span>Linen Management </span>
                      <span class="fa"></span>
                    </a>
                  <ul>
                    <li> <a href="${BASE_URL}/linen/linen-item-req.html"> Item Requisition </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-requisition-verify.html"> Requisition Verify </a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-issue.html"> Issue Item </a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-stock-list.html"> Stock List </a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-opening-stock.html"> Opening Stock </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-opening-stock-approval.html">Opening Stock Approval</a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-stock-reconciliation.html">Stock Reconciliation </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-stock-reconciliation-approve.html">Stock Reconciliation Approve </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-central-store-item-receive.html"> Central Store Item Receive </a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-sub-yearly-demand-generate.html"> Sub Store Yearly Demand List </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-yearly-demand-generator.html"> Yearly Demand Generate </a>  </li>
                    <li> <a href="${BASE_URL}/linen/linen-yearly-demand-list.html"> Yearly Demand List </a> </li>
                    <li> <a href="${BASE_URL}/linen/linen-condemn-receive.html"> Condemn Receive</a> </li>

                    <li> <a href="${BASE_URL}/linen/linen-sub-store-demand-list.html"> Sub Store Demand List </a>  </li>
                  </ul>
                </li>
            <li class="parent-menu-item" id="housekeeping-menu-parent">
                <a href="#"> 
                    <span class="icon icon-35x icon-housekeeping"> </span>
                    <span>Housekeeping</span> 
                    <span class="fa"></span>
                 </a>
                <ul>
                    <li class=""><a href="${BASE_URL}/house-keeping/1.0-hk-clean-req-sc-cleaning.html">Cleaning Requisition</a></li>
                    <li><a href="${BASE_URL}/house-keeping/2.0-hk-clean-req-sc-cleaning-ipd.html">Cleaning Requisition-IPD</a></li>
                    <li class="activated-submenu"><a href="">Setup</a>
                        <ul>
                            <li><a href="${BASE_URL}/house-keeping/1.0-setup-cleaning-task.html">cleaning Task </a></li>
                            <li><a href="${BASE_URL}/house-keeping/2.0-setup-particular-task.html">Particular Task </a></li>
                            <li><a href="${BASE_URL}/house-keeping/3.0-setup-cleaning-schedule.html">Cleaning Schedule </a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="hr-menu-parent"> 
                <a href="#"> 
                    <span class="icon icon-35x icon-hr"></span> 
                    <span>Human Resource </span> 
                    <span class="fa"></span>
                </a>
                <ul>
                    <li class=""> <a href="${BASE_URL}/hr/1-hr-employee-profile-demography.html"> Employee Profile </a> </li>
                    <li> <a href="${BASE_URL}/hr/2-hr-employee-leave-entry.html"> Employee Leave Entry </a> </li>
                    <li> <a href="${BASE_URL}/hr/6-hr-employee-leave-req.html"> Leave Requested List</a> </li>
                    <li> <a href="${BASE_URL}/hr/3-hr-annual-confidential-report.html"> Annual Confidential Report </a> </li>

                    <li class="activated-submenu"><a href="#"> Setup </a>
                        <ul>
                            <li> <a href="${BASE_URL}/hr/1-hr-setup-shift.html"> Shift Setup </a> </li>
                            <li> <a href="${BASE_URL}/hr/2-hr-setup-employee-roaster.html"> Employee Roaster </a> </li>
                            <li> <a href="${BASE_URL}/hr/3-hr-setup-working-task.html"> Working Task </a> </li>
                            <li> <a href="${BASE_URL}/hr/4-hr-setup-lookup.html"> Lookup </a> </li>
                            <li> <a href="${BASE_URL}/hr/5-hr-setup-medical-board-member.html"> Medical Board Member </a> </li>
                        </ul>
                    </li>

                </ul>
            </li>
            <li class="parent-menu-item" id="catering-menu-parent">
                <a href="#">
                    <span class="icon icon-35x icon-catering"></span>
                    <span>Catering Management </span> 
                    <span class="fa"></span>
                </a>
                <ul>
                  <li> <a href="${BASE_URL}/catering/catering-diet-req-entry.html"> Diet Requisition</a> </li>
                  <li> <a href="${BASE_URL}/catering/catering-raw-item-requisitin.html"> Raw Item Requisition</a>  </li>
                  <li> <a href="${BASE_URL}/catering/catering-diet-cancel.html"> Diet Cancel</a>  </li>
                  <li> <a href="${BASE_URL}/catering/catering-diet-summary.html"> Diet Summary</a></li>
                  
                  <li class="activated-submenu">
                    <a href="#"> Reports</a>
                    <ul>
                      <li> <a href="${BASE_URL}/catering/catering-report-list-daily-admitted-patient-count-peramiter.html"> Daily Admitted Patients Count </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-adm-pat-hospital-bed-peramiter.html"> Admitted Patient in Hospital </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-daily-reject-food-peramiter.html"> Daily Rejected Food </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-morning-bed-statement-peramiter.html"> Morning Bed Statement </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-ward-patient-sum-food-qty-peramiter.html"> Patient Summary of Food </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-regular-diet-slip-peramiter.html"> Regular Diet Slip </a>  </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-ward-diet-cancel-list-peramiter.html"> Diet Cancel List </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-food-wise-delivery-peramiter.html"> Item Wise Delivery List </a>  </li>
                      <li> <a href="${BASE_URL}/catering/catering-report-item-receive-peramiter.html"> Item Receive List </a>  </li>
                    </ul>
                  </li>
                  
                  <li class="activated-submenu">
                    <a href="">Setup</a>
                    <ul>
                      <li> <a href="${BASE_URL}/catering/catering-setup-lookup-entry.html"> Lookup Entry </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-setup-meal-plan.html"> Meal Plan Setup </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-setup-raw-item.html"> Raw Item Setup </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-setup-diet-menu.html"> Diet Menu Setup </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-setup-group-wise-raw-item.html"> Group Wise Raw Item Mapping Setup </a> </li>
                      <li> <a href="${BASE_URL}/catering/catering-setup-food-serving-time.html"> Food Serving Time Setup </a> </li>
                    </ul>
                  </li>
                </ul>
            </li>
            <li class="parent-menu-item" id="cssd-menu-parent">
                <a href="#">
                  <span class="icon icon-35x icon-cssd"> </span>
                  <span>CSSD </span>
                  <span class="fa"></span>
                </a>
                <ul>
                  <li> <a href="${BASE_URL}/cssd/1-cssd-sterilization-requisition.html"> Sterilization Requisition </a> </li>
                  <li> <a href="${BASE_URL}/cssd/2-cssd-sterilization-requisition-list.html"> Sterilization Requisition List </a> </li>
                  <li> <a href="${BASE_URL}/cssd/3-cssd-sterilization-item-requisition.html"> CSSD Item Requisition </a></li>
                  <li> <a href="${BASE_URL}/cssd/4-cssd-item-requisition-verify.html"> Requisition Verify </a></li>                               
                  <li> <a href="${BASE_URL}/cssd/5-cssd-issue-list.html">  Issue  List </a></li>                               
                  <li> <a href="${BASE_URL}/cssd/6-cssd-opening-stock.html"> Opening Stock </a></li>                               
                  <li> <a href="${BASE_URL}/cssd/7-cssd-opening-stock-approval.html"> Opening Stock Approval </a></li>                               
                  <li> <a href="${BASE_URL}/cssd/8-cssd-stock-list.html"> Stock List </a></li>                               
                </ul>
            </li> 
            <li class="parent-menu-item" id="equipment-menu-parent">
                        <a href="#">
                            <span class="icon icon-35x icon-equipment"> </span>
                            <span>Equipment </span>
                            <span class="fa"></span>
                        </a>
                        <ul class="submenu">
                            <li><a href="${BASE_URL}/equipment/1-equipment-equipment-requisition.html">Equipment Requisition</a></li>
                            <li><a href="${BASE_URL}/equipment/2-equipment-requisition-verify.html">Equipment Requisition Verify</a></li>
                            <li><a href="${BASE_URL}/equipment/3-equipment-issue-item-list.html">Issue Item List</a></li>
                            <li><a href="${BASE_URL}/equipment/4-equipment-complain.html">Equipment Complain</a></li>
                            <li><a href="${BASE_URL}/equipment/5-equipment-complain-worklist.html">Equipment Complain Worklist</a></li>
                            <li><a href="${BASE_URL}/equipment/6-equipment-complain-verify.html">Equipment Complain Verify</a></li>
                            <li class="activated-submenu">
                                <a href="">Setup</a>
                                <ul>
                                    <li><a href="${BASE_URL}/equipment/21-equipment-setup-machine.html">Machine Setup</a></li>
                                    <li><a href="${BASE_URL}/equipment/22-equipment-setup-equipment-issue-labeling.html">Machine Issue Labeling</a></li>
                                    <li><a href="${BASE_URL}/equipment/23-equipment-setup-equipment-maintanance.html">Equipment Maintanance</a></li>
                                    <li><a href="${BASE_URL}/equipment/24-equipment-equipment-servicing-details.html">Equipment Servicing Details</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
            <li class="parent-menu-item" id="labor-room-menu-parent"> 
                 <a href="#"> 
                     <span class="icon icon-35x icon-labor-room"></span>
                     <span>Labor Room</span>
                     <span class="fa"></span>
                 </a>
                <ul>
                    <li> <a href="${BASE_URL}/labor-room/admission-html/labor-room-adm-req-list.html">  Admission </a> </li> 
                    <li> <a href="${BASE_URL}/labor-room/admission-html/labor-room-birth-registration.html"> Birth Registration </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/admission-html/labor-room-baby-admission.html"> Baby Admission </a> </li>  
                    <li> <a href="${BASE_URL}/labor-room/nurse-html/01-labor-room-ns-patient-rcv-rqst.html"> Nurse Station </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/nurse-html/55-labor-room-ns-bbr-return.html"> Blood Return </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/nurse-html/61-labor-room-item-consumption.html"> Medicine Consumption </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/nurse-html/63-labor-room-item-consumption.html"> Consumption (Others)</a> </li>
                    <li> <a href="${BASE_URL}/labor-room/nurse-html/62-labor-room-ns-dis-req-patient-list.html"> Discharge Requested Patient List </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/doctor-html/01.0-ipd-ds-adm-patient-list.html"> Doctor Station </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/business-ofc-html/ipd-business-patient-list.html"> Bill Counter </a> </li>
                    <li> <a href="${BASE_URL}/labor-room/business-ofc-html/bill-settlement.html"> Financial Settlement</a> </li>
                    <li> <a href="${BASE_URL}/labor-room/business-ofc-html/advance-deposit-collection.html"> Advance Collection</a> </li>
              
                <li class="activated-submenu">
                <a href="">OT</a>
                <ul>
                <li><a href="${BASE_URL}/labor-room/ot/01.0-ot-otrq-rql.html">OT Requisition</a></li>
                <li><a href="${BASE_URL}/labor-room/ot/16.0-OT-OTSH.html">OT Room Schedule</a></li>
              </ul>
            </li>
            </ul>
            </li>
            <li class="parent-menu-item" id="procurement-menu-parent"> 
            <a href="#"> <span class="icon icon-35x icon-procurement"></span> <span>Procurement  </span> <span class="fa"></span> </a>
                <ul>
                    <li><a href="${BASE_URL}/procurement/1-procurement-purchase-order-generate.html">Purchase Order Generate</a></li>
                    <li><a href="${BASE_URL}/procurement/2-procurement-purchase-order-approval.html">Purchase Order Approval</a></li>
                    <li><a href="${BASE_URL}/procurement/4-procurement-invoice-generate.html">Invoice Generate</a></li>
                    <li><a href="${BASE_URL}/procurement/5-procurement-supplier-payment.html">Supplier Payment</a></li>
                </ul>
            </li>
            <li class="parent-menu-item" id="record-room-menu-parent"> 
                 <a href="#"> 
                     <span class="icon icon-35x icon-record-room"></span>
                     <span>Record Room</span>
                     <span class="fa"></span>
                 </a>
                 <ul>
                 <li><a href="${BASE_URL}/record-room/1-record-rm-patient-list.html">Patient List</a></li>
                 <li><a href="${BASE_URL}/record-room/1.1-record-rm-register.html">Patient Register</a></li>
                 <li><a href="${BASE_URL}/record-room/3-record-rm-cert-register.html">Certificate Issue Register</a></li>
                 <li><a href="${BASE_URL}/record-room/4-record-rm-patient-retrival-req.html">Patient's File Retrival Request</a></li>
                 <li><a href="${BASE_URL}/record-room/4.2-record-rm-patient-retrival-req-list.html">Patient's File Retrival Request List</a></li>
                 
                 <li class="activated-submenu"><a href="#">Document Management</a>
                 <ul>
                     <li><a href="${BASE_URL}/record-room/6-record-rm-app-rec-doc.html">Record Document</a> </li>
                     <li><a href="${BASE_URL}/record-room/7-record-rm-doc-list.html">Document  Worklist</a></li>
                 </ul>
                   
                 </li>

             </ul>
            </li>            
            <li class="parent-menu-item" id="store-menu-parent"> 
                 <a href="#"> 
                     <span class="icon icon-35x icon-store"></span>
                     <span>Store</span>
                     <span class="fa"></span>
                 </a>
                <ul>
                    <li><a href="${BASE_URL}/store/1-store-item-requisition-to-central-store.html">  Item Requisition To Central Store </a> </li> 
                    <li><a href="${BASE_URL}/store/2-store-requisition-verify.html">Requisition Verify</a></li>
                    <li><a href="${BASE_URL}/store/3-store-issue-item-list.html">Issue Item List</a></li>
                    <li><a href="${BASE_URL}/store/4-store-opening-stock.html">Opening Stock</a></li>
                    <li><a href="${BASE_URL}/store/5-store-opening-stock-list.html">Opening Stock List</a></li>
                    <li><a href="${BASE_URL}/store/6-store-opening-stock-approval.html">Opening Stock Approval</a></li>
                    <li><a href="${BASE_URL}/store/7-store-stock-reconciliation.html">Stock Reconciliation</a></li>
                    <li><a href="${BASE_URL}/store/8-store-stock-reconciliation-list.html">Stock Reconciliation List</a></li>
                    <li><a href="${BASE_URL}/store/9-store-central-store-item-receive.html"> Central Store Item Receive</a></li>
                    <li><a href="${BASE_URL}/store/10-store-central-item-receive-list.html">Central Store Item Receive List</a></li>
                    <li><a href="${BASE_URL}/store/11-store-sub-store-yearly-demand-generate.html">Sub Store Yearly Demand Generate</a></li>
                    <li><a href="${BASE_URL}/store/12-store-sub-store-yearly-demand-list.html">Sub Store Yearly Demand List </a></li>
                    <li><a href="${BASE_URL}/store/13-store-yearly-demand-generate.html">Yearly Demand Generate</a></li>
                    <li><a href="${BASE_URL}/store/14-store-yearly-demand-list.html">Yearly Demand List</a></li>
                    <li><a href="${BASE_URL}/store/15-store-sub-store-demand-list.html">Sub Store Yearly Demand List</a></li>               
                </ul>
            </li>        
            <li class="parent-menu-item" id="budget-menu-parent"> 
                 <a href="#"> 
                     <span class="icon icon-35x icon-store"></span>
                     <span>Budget</span>
                     <span class="fa"></span>
                 </a>
                <ul>
                    <li><a href="${BASE_URL}/budget/budget-entry.html">  Budget Entry  </a> </li> 
                    <li><a href="${BASE_URL}/budget/fin-head-entry.html">  Financial Head Entry  </a> </li> 
                    
                </ul>
            </li>        
        </ul>`;

  jQuery(".leftside-menu-wrapper").html(leftMenu);
  // jQuery(`a[href="${url}"]`).css({
  //     'color': 'red'
  // });
  jQuery(`a[href="${url}"]`).parent().addClass("activated-submenu");
  jQuery(document).on(
    "click",
    ".leftside-menu-wrapper > ul > li > ul > li  a",
    function () {
      let parentMenuItem = jQuery(this)
        .parents("li.parent-menu-item")
        .attr("id");
      localStorage.setItem("current-module", parentMenuItem);
    }
  );
  let currentModule = localStorage.getItem("current-module");
  jQuery(`#${currentModule}`).addClass("activated");
});
